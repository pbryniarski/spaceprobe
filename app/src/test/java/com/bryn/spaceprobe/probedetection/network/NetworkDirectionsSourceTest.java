package com.bryn.spaceprobe.probedetection.network;

import android.support.annotation.NonNull;

import com.bryn.spaceprobe.probedetection.movement.Direction;
import com.bryn.spaceprobe.probedetection.network.NetworkDirectionsSource;
import com.bryn.spaceprobe.probedetection.network.SpaceProbeApi;
import com.bryn.spaceprobe.probedetection.network.serverresponse.Directions;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.LinkedList;
import java.util.List;

import rx.Observable;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

/**
 * Created by pawelbryniarski on 27.10.15.
 */
public class NetworkDirectionsSourceTest {

    private final String STUB_EMAIL = "email";
    @Mock
    SpaceProbeApi api;

    private Directions directions;

    private NetworkDirectionsSource source;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        source = new NetworkDirectionsSource(api, STUB_EMAIL);
        directions = getDirections();
    }

    @Test
    public void testGetDirections() {
        doReturn(Observable.just(directions)).when(api).getDirections(anyString());
        TestSubscriber<Direction> subscriber = new TestSubscriber<>();
        source.getDirections().subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        subscriber.assertCompleted();
        subscriber.assertNoErrors();
        subscriber.assertValueCount(directions.directionsList.size());

        checkIfDirectionsCorrect(subscriber, directions.directionsList);

        verify(api).getDirections(eq(STUB_EMAIL));
    }

    private void checkIfDirectionsCorrect(TestSubscriber<Direction> subscriber, List<String> stringDirections) {
        assertEquals(stringDirections.size(), subscriber.getOnNextEvents().size());
        for (int i = 0, size = stringDirections.size(); i < size; i++) {
            assertEquals(Direction.valueOf(stringDirections.get(i)),
                    subscriber.getOnNextEvents().get(i));
        }
    }

    @NonNull
    private Directions getDirections() {
        List<String> dirs = new LinkedList<>();
        dirs.add("FORWARD");
        dirs.add("RIGHT");
        dirs.add("LEFT");
        dirs.add("FORWARD");
        Directions directions = new Directions();
        directions.directionsList = dirs;
        return directions;
    }
}
