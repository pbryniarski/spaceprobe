package com.bryn.spaceprobe.probedetection.network.serverresponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by pawelbryniarski on 04.11.15.
 */
public class SubmitResponse {

    @SerializedName("Message")
    public String message;

    @SerializedName("StatusCode")
    public int statusCode;

    public SubmitResponse(String message, int statusCode) {
        this.message = message;
        this.statusCode = statusCode;
    }
}
