package com.bryn.spaceprobe.probedetection.movement;

/**
 * Created by pawelbryniarski on 31.10.15.
 */
public enum Direction {
    FORWARD("FORWARD"),
    LEFT("LEFT"),
    RIGHT("RIGHT");

    private String strDirection;

    Direction(String strDirection) {
        this.strDirection = strDirection;
    }

    public static Direction fromString(String directionName) {
        if (directionName != null) {
            for (Direction direction : Direction.values()) {
                if (directionName.equals(direction.strDirection)) {
                    return direction;
                }
            }
        }
        throw new IllegalArgumentException("Direction: " + directionName + " not found ");
    }
}
