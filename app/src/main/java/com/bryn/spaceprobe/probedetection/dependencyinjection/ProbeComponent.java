package com.bryn.spaceprobe.probedetection.dependencyinjection;

import com.bryn.spaceprobe.probedetection.ProbePositionCalculatorFactory;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = SpaceProbeModule.class)
public interface ProbeComponent {
    void inject(ProbePositionCalculatorFactory probePositionCalculatorFactory);
}
