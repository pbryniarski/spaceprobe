package com.bryn.spaceprobe.probedetection.movement;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by pawelbryniarski on 27.10.15.
 */
public class ProbeStateTest {

    @Test
    public void startProbeState() {
        ProbeState probeState = ProbeState.getStartProbeState();

        assertEquals(0, probeState.getX());
        assertEquals(0, probeState.getY());
        assertEquals(ProbeOrientation.UP, probeState.getOrientation());
    }

    @Test
    public void probeStateMoveX() {
        ProbeState probeState = ProbeState.getStartProbeState();

        ProbeState movedProbeState = probeState.moveX(1);

        assertEquals(1, movedProbeState.getX());
        assertEquals(0, movedProbeState.getY());
        assertEquals(ProbeOrientation.UP, movedProbeState.getOrientation());
    }

    @Test(expected = IllegalStateException.class)
    public void probeStateMoveXIncorrect() {
        ProbeState probeState = ProbeState.getStartProbeState();

        probeState.moveX(-1);
    }

    @Test(expected = IllegalStateException.class)
    public void probeStateMoveXIncorrect2() {
        ProbeState probeState = ProbeState.getStartProbeState();

        probeState.moveX(100);
    }

    @Test
    public void probeStateMoveY() {
        ProbeState probeState = ProbeState.getStartProbeState();

        ProbeState movedProbeState = probeState.moveY(1);

        assertEquals(0, movedProbeState.getX());
        assertEquals(1, movedProbeState.getY());
        assertEquals(ProbeOrientation.UP, movedProbeState.getOrientation());
    }

    @Test(expected = IllegalStateException.class)
    public void probeStateMoveYIncorrect() {
        ProbeState probeState = ProbeState.getStartProbeState();

        probeState.moveY(-1);
    }

    @Test(expected = IllegalStateException.class)
    public void probeStateMoveYIncorrect2() {
        ProbeState probeState = ProbeState.getStartProbeState();

        probeState.moveY(100);
    }

    @Test
    public void changeProbeStateDirection() {
        ProbeState probeState = ProbeState.getStartProbeState();

        probeState = probeState.changeOrientation(ProbeOrientation.DOWN);

        assertEquals(ProbeOrientation.DOWN, probeState.getOrientation());
    }


}
