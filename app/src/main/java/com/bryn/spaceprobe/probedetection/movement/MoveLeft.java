package com.bryn.spaceprobe.probedetection.movement;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by pawelbryniarski on 29.10.15.
 */
@Singleton
public class MoveLeft implements Move {

    private final Map<ProbeOrientation, ProbeOrientation> leftTransformationMap;

    @Inject
    MoveLeft() {
        leftTransformationMap = new HashMap<>();
        leftTransformationMap.put(ProbeOrientation.UP, ProbeOrientation.LEFT);
        leftTransformationMap.put(ProbeOrientation.DOWN, ProbeOrientation.RIGHT);
        leftTransformationMap.put(ProbeOrientation.LEFT, ProbeOrientation.DOWN);
        leftTransformationMap.put(ProbeOrientation.RIGHT, ProbeOrientation.UP);
    }

    @Override
    public ProbeState move(ProbeState probeState) {
        return probeState.changeOrientation(leftTransformationMap.get(probeState.getOrientation()));
    }
}
