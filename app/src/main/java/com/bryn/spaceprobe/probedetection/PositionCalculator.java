package com.bryn.spaceprobe.probedetection;

import com.bryn.spaceprobe.probedetection.movement.Mover;
import com.bryn.spaceprobe.probedetection.movement.ProbeState;
import com.bryn.spaceprobe.probedetection.network.DirectionsSource;
import com.bryn.spaceprobe.probedetection.network.PositionSender;

import javax.inject.Inject;

import rx.Observable;
import rx.schedulers.Schedulers;

public class PositionCalculator {

    private final DirectionsSource directionsSource;
    private final PositionSender sender;
    private final Mover mover;


    @Inject
    public PositionCalculator(DirectionsSource directionsSource, PositionSender sender, Mover mover) {
        this.directionsSource = directionsSource;
        this.sender = sender;
        this.mover = mover;
    }

    public Observable<String> calculateAndSendPosition() {
        return directionsSource.getDirections()
                .observeOn(Schedulers.computation())
                .reduce(ProbeState.getStartProbeState(), mover)
                .observeOn(Schedulers.io())
                .flatMap(sender);
    }
}
