package com.bryn.spaceprobe.probedetection.network.serverresponse;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Directions {

    @SerializedName("Directions")
    public List<String> directionsList;
}
