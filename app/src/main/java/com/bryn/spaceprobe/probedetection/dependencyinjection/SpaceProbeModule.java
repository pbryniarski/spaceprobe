package com.bryn.spaceprobe.probedetection.dependencyinjection;

import com.bryn.spaceprobe.probedetection.network.DirectionsSource;
import com.bryn.spaceprobe.probedetection.network.NetworkDirectionsSource;
import com.bryn.spaceprobe.probedetection.network.PositionSender;
import com.bryn.spaceprobe.probedetection.network.SpaceProbeApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

@Module
public class SpaceProbeModule {

    private final String API_EMAIL = "bryniarskip";

    @Provides
    @Singleton
    DirectionsSource getDirectionsSource(SpaceProbeApi spaceProbeApi) {
        return new NetworkDirectionsSource(spaceProbeApi, API_EMAIL);
    }

    @Provides
    @Singleton
    SpaceProbeApi getSpaceProbeApi() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://mands-alien-test.herokuapp.com")
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(SpaceProbeApi.class);
    }

    @Provides
    @Singleton
    PositionSender getPositionSender(SpaceProbeApi spaceProbeApi) {
        return new PositionSender(API_EMAIL, spaceProbeApi);
    }
}
