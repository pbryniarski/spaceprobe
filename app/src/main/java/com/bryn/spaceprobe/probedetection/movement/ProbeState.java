package com.bryn.spaceprobe.probedetection.movement;

import android.support.annotation.VisibleForTesting;

/**
 * Created by pawelbryniarski on 29.10.15.
 */
public class ProbeState {

    /**
     * Lets assume universe looks like that:
     *
     * y
     *
     * 9
     * 8
     * 7
     * 6
     * 5
     * 4
     * 3
     * 2
     * 1
     * 0 ^
     * 0 1 2 3 4 5 6 7 8 9   x
     * Orientation UP;
     */

    private static final int MAX_X_POSITION = 9;
    private static final int MIN_X_POSITION = 0;

    private static final int MAX_Y_POSITION = 9;
    private static final int MIN_Y_POSITION = 0;

    private final int x;
    private final int y;
    private final ProbeOrientation orientation;

    @VisibleForTesting
    ProbeState(int x, int y, ProbeOrientation orientation) {
        this.x = x;
        this.y = y;
        this.orientation = orientation;
    }

    private ProbeState() {
        this.x = 0;
        this.y = 0;
        this.orientation = ProbeOrientation.UP;
    }

    public static ProbeState getStartProbeState() {
        return new ProbeState();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    protected ProbeState move(Move move) {
        return move.move(this);
    }

    protected ProbeOrientation getOrientation() {
        return orientation;
    }

    protected ProbeState changeOrientation(ProbeOrientation orientation) {
        return this.withNewDirection(orientation);
    }

    protected ProbeState moveY(int distance) {
        int newY = this.y + distance;
        if (newY > MAX_Y_POSITION || newY < MIN_Y_POSITION) {
            throw new IllegalStateException("New Y position is incorrect current Y: " + this.y + " new Y: " + newY);
        }
        return this.withNewY(newY);
    }

    protected ProbeState moveX(int distance) {
        int newX = this.x + distance;
        if (newX > MAX_X_POSITION || newX < MIN_X_POSITION) {
            throw new IllegalStateException("New X position is incorrect current X: " + this.x + " new X: " + newX);
        }
        return this.withNewX(newX);
    }

    private ProbeState withNewDirection(ProbeOrientation orientation) {
        return new ProbeState(this.x, this.y, orientation);
    }

    private ProbeState withNewY(int newY) {
        return new ProbeState(this.x, newY, this.orientation);
    }

    private ProbeState withNewX(int newX) {
        return new ProbeState(newX, this.y, this.orientation);
    }
}
