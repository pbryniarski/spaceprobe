package com.bryn.spaceprobe.probedetection.movement;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

/**
 * Created by pawelbryniarski on 27.10.15.
 */
public class MoveForwardTest {

    @Mock
    ProbeState probeState;

    private MoveForward moveForward;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        moveForward = new MoveForward();
    }

    @Test
    public void forwardDirectionUp() {
        doReturn(ProbeOrientation.UP).when(probeState).getOrientation();

        moveForward.move(probeState);

        verify(probeState).moveY(eq(1));
    }

    @Test
    public void forwardDirectionDown() {
        doReturn(ProbeOrientation.DOWN).when(probeState).getOrientation();

        moveForward.move(probeState);

        verify(probeState).moveY(eq(-1));
    }

    @Test
    public void forwardDirectionRight() {
        doReturn(ProbeOrientation.RIGHT).when(probeState).getOrientation();

        moveForward.move(probeState);

        verify(probeState).moveX(eq(1));
    }

    @Test
    public void forwardDirectionLeft() {
        doReturn(ProbeOrientation.LEFT).when(probeState).getOrientation();

        moveForward.move(probeState);

        verify(probeState).moveX(eq(-1));
    }
}
