package com.bryn.spaceprobe.probedetection;

import com.bryn.spaceprobe.probedetection.dependencyinjection.DaggerProbeComponent;
import com.bryn.spaceprobe.probedetection.dependencyinjection.ProbeComponent;
import com.bryn.spaceprobe.probedetection.dependencyinjection.SpaceProbeModule;

import javax.inject.Inject;

public class ProbePositionCalculatorFactory {

    @Inject
    PositionCalculator calculator;

    public ProbePositionCalculatorFactory() {
        ProbeComponent component =  DaggerProbeComponent.builder().spaceProbeModule(new SpaceProbeModule()).build();
        component.inject(this);
    }

    public PositionCalculator getCalculator() {
        return calculator;
    }
}
