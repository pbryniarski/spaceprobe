package com.bryn.spaceprobe.probedetection.movement;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Collection;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(Parameterized.class)
public class MoveLeftTest {

    @Parameterized.Parameters
    public static Collection directionsBeforeAfter() {
        return Arrays.asList(new Object[][]{
                {ProbeOrientation.UP, ProbeOrientation.LEFT},
                {ProbeOrientation.DOWN, ProbeOrientation.RIGHT},
                {ProbeOrientation.RIGHT, ProbeOrientation.UP},
                {ProbeOrientation.LEFT, ProbeOrientation.DOWN}
        });
    }

    private final ProbeOrientation startDirection;
    private final ProbeOrientation endDirection;

    @Mock
    ProbeState probeState;

    private MoveLeft leftTransformation;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        leftTransformation = new MoveLeft();
        doReturn(startDirection).when(probeState).getOrientation();
    }

    public MoveLeftTest(ProbeOrientation startDirection, ProbeOrientation endDirection) {
        this.startDirection = startDirection;
        this.endDirection = endDirection;
    }

    @Test
    public void testCorrectDirectionAfterLeftTransform() {
        leftTransformation.move(probeState);
        verify(probeState).changeOrientation(endDirection);
    }
}
