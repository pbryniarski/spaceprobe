package com.bryn.spaceprobe.probedetection.network;

import com.bryn.spaceprobe.probedetection.movement.ProbeState;
import com.bryn.spaceprobe.probedetection.network.serverresponse.SubmitResponse;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by pawelbryniarski on 04.11.15.
 */
public class PositionSender implements Func1<ProbeState, Observable<String>> {

    protected static final int STATUS_CODE_OK = 200;
    protected static final int STATUS_CODE_ERROR = 500;

    private final String email;
    private final SpaceProbeApi api;


    public PositionSender(String email, SpaceProbeApi api) {
        this.email = email;
        this.api = api;
    }

    @Override
    public Observable<String> call(ProbeState probeState) {
        return api.sendFinalPosition(email, probeState.getX(), probeState.getY()).map(new Func1<SubmitResponse, String>() {
            @Override
            public String call(SubmitResponse submitResponse) {
                if (submitResponse.statusCode == STATUS_CODE_OK) {
                    return submitResponse.message;
                } else if (submitResponse.statusCode == STATUS_CODE_ERROR) {
                    throw new RuntimeException(submitResponse.message);
                } else {
                    throw new IllegalStateException("Unable to recognize server status code. Code: " +
                            submitResponse.statusCode + " Message: " + submitResponse.message);
                }
            }
        });
    }
}
