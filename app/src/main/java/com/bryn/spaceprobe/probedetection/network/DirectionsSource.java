package com.bryn.spaceprobe.probedetection.network;

import com.bryn.spaceprobe.probedetection.movement.Direction;

import rx.Observable;

/**
 * Created by pawelbryniarski on 27.10.15.
 */
public interface DirectionsSource {
    Observable<Direction> getDirections();
}
