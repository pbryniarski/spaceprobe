package com.bryn.spaceprobe.probedetection.movement;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import rx.functions.Func2;

/**
 * Created by pawelbryniarski on 29.10.15.
 */
public class Mover implements Func2<ProbeState, Direction, ProbeState> {

    private final Map<Direction, Move> transformations = new HashMap<>();

    @Inject
    Mover(MoveLeft leftTransformation,
          MoveRight moveRight,
          MoveForward moveForward) {
        transformations.put(Direction.LEFT, leftTransformation);
        transformations.put(Direction.RIGHT, moveRight);
        transformations.put(Direction.FORWARD, moveForward);
    }

    @Override
    public ProbeState call(ProbeState probeState, Direction move) {
        return probeState.move(transformations.get(move));
    }
}
