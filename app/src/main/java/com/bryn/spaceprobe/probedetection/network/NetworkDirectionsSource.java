package com.bryn.spaceprobe.probedetection.network;

import com.bryn.spaceprobe.probedetection.movement.Direction;
import com.bryn.spaceprobe.probedetection.network.serverresponse.Directions;

import rx.Observable;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by pawelbryniarski on 27.10.15.
 */
public class NetworkDirectionsSource implements DirectionsSource {

    private final SpaceProbeApi spaceProbeApi;
    private final String email;

    public NetworkDirectionsSource(SpaceProbeApi spaceProbeApi, String email) {
        this.spaceProbeApi = spaceProbeApi;
        this.email = email;
    }

    @Override
    public Observable<Direction> getDirections() {
        return spaceProbeApi
                .getDirections(email)
                .subscribeOn(Schedulers.io())
                .flatMap(new Func1<Directions, Observable<String>>() {
                    @Override
                    public Observable<String> call(Directions directions) {
                        return Observable.from(directions.directionsList);
                    }
                })
                .map(new Func1<String, Direction>() {
                    @Override
                    public Direction call(String s) {
                        return Direction.valueOf(s);
                    }
                });
    }
}
