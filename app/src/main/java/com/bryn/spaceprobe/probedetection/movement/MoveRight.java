package com.bryn.spaceprobe.probedetection.movement;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by pawelbryniarski on 29.10.15.
 */
@Singleton
public class MoveRight implements Move {

    private final Map<ProbeOrientation, ProbeOrientation> rightTransformationMap;

    @Inject
    MoveRight() {
        rightTransformationMap = new HashMap<>();
        rightTransformationMap.put(ProbeOrientation.UP, ProbeOrientation.RIGHT);
        rightTransformationMap.put(ProbeOrientation.DOWN, ProbeOrientation.LEFT);
        rightTransformationMap.put(ProbeOrientation.LEFT, ProbeOrientation.UP);
        rightTransformationMap.put(ProbeOrientation.RIGHT, ProbeOrientation.DOWN);
    }

    @Override
    public ProbeState move(ProbeState probeState) {
        return probeState.changeOrientation(rightTransformationMap.get(probeState.getOrientation()));
    }
}
