package com.bryn.spaceprobe.probedetection.movement;

/**
 * Created by pawelbryniarski on 10.11.15.
 */
public enum  ProbeOrientation {

//    public static final int UP = 0;
//    public static final int DOWN = 1;
//    public static final int LEFT = 2;
//    public static final int RIGHT = 3;
//
    UP,
    DOWN,
    LEFT,
    RIGHT
//
//    FORWARD("FORWARD"),
//    LEFT("LEFT"),
//    RIGHT("RIGHT");
//
//    private String strDirection;
//
//    Direction(String strDirection) {
//        this.strDirection = strDirection;
//    }
//
//    public static Direction fromString(String directionName) {
//        if (directionName != null) {
//            for (Direction direction : Direction.values()) {
//                if (directionName.equals(direction.strDirection)) {
//                    return direction;
//                }
//            }
//        }
//        throw new IllegalArgumentException("Direction: " + directionName + " not found ");
//    }
}
