package com.bryn.spaceprobe.probedetection.movement;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Collection;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(Parameterized.class)
public class MoveRightTest {

    @Parameterized.Parameters
    public static Collection directionsBeforeAfter() {
        return Arrays.asList(new Object[][]{
                {ProbeOrientation.UP, ProbeOrientation.RIGHT},
                {ProbeOrientation.DOWN, ProbeOrientation.LEFT},
                {ProbeOrientation.RIGHT, ProbeOrientation.DOWN},
                {ProbeOrientation.LEFT, ProbeOrientation.UP}
        });
    }

    private final ProbeOrientation startDirection;
    private final ProbeOrientation endDirection;

    @Mock
    ProbeState probeState;

    private MoveRight moveRight;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        moveRight = new MoveRight();
        doReturn(startDirection).when(probeState).getOrientation();
    }

    public MoveRightTest(ProbeOrientation startDirection, ProbeOrientation endDirection) {
        this.startDirection = startDirection;
        this.endDirection = endDirection;
    }

    @Test
    public void testCorrectDirectionAfterRightTransform() {
        moveRight.move(probeState);
        verify(probeState).changeOrientation(endDirection);
    }
}
