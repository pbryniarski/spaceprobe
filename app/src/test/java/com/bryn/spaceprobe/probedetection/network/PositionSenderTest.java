package com.bryn.spaceprobe.probedetection.network;

import com.bryn.spaceprobe.probedetection.movement.ProbeState;
import com.bryn.spaceprobe.probedetection.network.PositionSender;
import com.bryn.spaceprobe.probedetection.network.SpaceProbeApi;
import com.bryn.spaceprobe.probedetection.network.serverresponse.SubmitResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import rx.Observable;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;

/**
 * Created by pawelbryniarski on 04.11.15.
 */
public class PositionSenderTest {

    private final String STUB_EMAIL = "email";

    @Mock
    SpaceProbeApi api;

    private SubmitResponse submitResponse;

    private PositionSender sender;

    private TestSubscriber<String> subscriber;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        sender = new PositionSender(STUB_EMAIL, api);
        subscriber = new TestSubscriber<>();
    }

    @Test
    public void testSuccess() {
        String message = "Message success";
        submitResponse = new SubmitResponse(message, PositionSender.STATUS_CODE_OK);
        doReturn(Observable.just(submitResponse)).when(api).sendFinalPosition(anyString(), anyInt(), anyInt());

        sender.call(ProbeState.getStartProbeState())
                .subscribe(subscriber);

        subscriber.awaitTerminalEvent();
        subscriber.assertNoErrors();
        subscriber.assertValueCount(1);
        subscriber.assertValue(message);
    }

    @Test
    public void testError500() {
        String message = "Error 500";
        submitResponse = new SubmitResponse(message, PositionSender.STATUS_CODE_ERROR);
        doReturn(Observable.just(submitResponse)).when(api).sendFinalPosition(anyString(), anyInt(), anyInt());

        sender.call(ProbeState.getStartProbeState())
                .subscribe(subscriber);

        subscriber.awaitTerminalEvent();
        assertEquals(1, subscriber.getOnErrorEvents().size());
        RuntimeException exception = (RuntimeException) subscriber.getOnErrorEvents().get(0);
        assertEquals(message, exception.getMessage());
    }

    @Test
    public void testErrorUndefined() {
        String message = "Error 1500";
        submitResponse = new SubmitResponse(message, 1500);
        doReturn(Observable.just(submitResponse)).when(api).sendFinalPosition(anyString(), anyInt(), anyInt());

        sender.call(ProbeState.getStartProbeState())
                .subscribe(subscriber);

        subscriber.awaitTerminalEvent();
        assertEquals(1, subscriber.getOnErrorEvents().size());
        IllegalStateException exception = (IllegalStateException) subscriber.getOnErrorEvents().get(0);
        assertTrue(exception.getMessage().contains(message));
    }
}
