package com.bryn.spaceprobe.probedetection.movement;

/**
 * Created by pawelbryniarski on 29.10.15.
 */
public interface Move {
    ProbeState move(ProbeState probeState);
}
