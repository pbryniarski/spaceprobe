package com.bryn.spaceprobe.probedetection.movement;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by pawelbryniarski on 29.10.15.
 */
@Singleton
public class MoveForward implements Move {

    @Inject
    MoveForward() {
    }

    @Override
    public ProbeState move(ProbeState probeState) {
        ProbeOrientation orientation= probeState.getOrientation();
        switch (orientation) {
            case DOWN:
                return probeState.moveY(-1);
            case UP:
                return probeState.moveY(1);
            case LEFT:
                return probeState.moveX(-1);
            case RIGHT:
                return probeState.moveX(1);
        }
        throw new IllegalStateException("Probe direction incorrect, unable to move forward");
    }
}
