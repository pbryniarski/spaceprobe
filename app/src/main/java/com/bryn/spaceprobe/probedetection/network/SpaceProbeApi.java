package com.bryn.spaceprobe.probedetection.network;

import com.bryn.spaceprobe.probedetection.network.serverresponse.Directions;
import com.bryn.spaceprobe.probedetection.network.serverresponse.SubmitResponse;

import retrofit.http.GET;
import retrofit.http.Path;
import rx.Observable;

public interface SpaceProbeApi {
    @GET("/api/spaceprobe/getdata/{email}")
    Observable<Directions> getDirections(@Path("email") String email);


    @GET("/api/spaceprobe/submitdata/{email}/{x}/{y}")
    Observable<SubmitResponse> sendFinalPosition(@Path("email") String email,
                                                 @Path("x") int x,
                                                 @Path("y") int y);
}
