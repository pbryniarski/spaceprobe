package com.bryn.spaceprobe.probedetection.movement;

import android.support.annotation.NonNull;

import com.bryn.spaceprobe.probedetection.network.DirectionsSource;
import com.bryn.spaceprobe.probedetection.network.NetworkDirectionsSource;
import com.bryn.spaceprobe.probedetection.network.SpaceProbeApi;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.mockwebserver.MockResponse;
import com.squareup.okhttp.mockwebserver.MockWebServer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import rx.observers.TestSubscriber;
import rx.schedulers.Schedulers;

import static junit.framework.Assert.assertEquals;

@RunWith(Parameterized.class)
public class CalculateFunctionalTest {

    private static final String responseBody0 = "{\"Directions\":[\"FORWARD\",\"FORWARD\",\"FORWARD\",\"RIGHT\",\"FORWARD\"]}";
    private static final ProbeState FINAL_PROBE_STATE_0 = new ProbeState(1, 3, ProbeOrientation.RIGHT);
    private static final String responseBody1 = "{\"Directions\":[\"FORWARD\",\"RIGHT\",\"FORWARD\",\"RIGHT\",\"FORWARD\"]}";
    private static final ProbeState FINAL_PROBE_STATE_1 = new ProbeState(1, 0, ProbeOrientation.DOWN);
    private static final String responseBody2 = "{\"Directions\":[\"RIGHT\",\"FORWARD\",\"LEFT\",\"FORWARD\",\"FORWARD\"]}";
    private static final ProbeState FINAL_PROBE_STATE_2 = new ProbeState(1, 2, ProbeOrientation.UP);

    @Parameterized.Parameters
    public static Collection directionsBeforeAfter() {
        return Arrays.asList(new Object[][]{
                {responseBody0, FINAL_PROBE_STATE_0},
                {responseBody1, FINAL_PROBE_STATE_1},
                {responseBody2, FINAL_PROBE_STATE_2}
        });
    }

    private String responseBody;
    private ProbeState finalProbeState;

    public CalculateFunctionalTest(String responseBody, ProbeState finalProbeState) {
        this.responseBody = responseBody;
        this.finalProbeState = finalProbeState;
    }

    private MockWebServer server;
    private TestSubscriber<ProbeState> positionTestSubscriber;
    DirectionsSource source;
    Mover mover;

    @Before
    public void setUp() throws IOException {
        server = prepareMockWebServer();
        HttpUrl baseUrl = server.url("");
        getPositionCalculator(baseUrl);
        positionTestSubscriber = new TestSubscriber<>();
    }

    @After
    public void tearDown() throws IOException {
        server.shutdown();
    }

    @Test
    public void testGetDirections() throws IOException {

        source.getDirections()
                .observeOn(Schedulers.computation())
                .reduce(ProbeState.getStartProbeState(), mover)
                .subscribe(positionTestSubscriber);

        positionTestSubscriber.awaitTerminalEvent();
        positionTestSubscriber.assertCompleted();
        positionTestSubscriber.assertNoErrors();
        assertEquals(1, positionTestSubscriber.getOnNextEvents().size());
        ProbeState calculatedProbeState = positionTestSubscriber.getOnNextEvents().get(0);
        assertEquals(finalProbeState.getX(), calculatedProbeState.getX());
        assertEquals(finalProbeState.getY(), calculatedProbeState.getY());
        assertEquals(finalProbeState.getOrientation(), calculatedProbeState.getOrientation());
    }

    @NonNull
    private MockWebServer prepareMockWebServer() throws IOException {
        MockWebServer server = new MockWebServer();
        server.enqueue(new MockResponse().setBody(responseBody));
        server.start();
        return server;
    }

    @NonNull
    private void getPositionCalculator(HttpUrl baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl.toString())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SpaceProbeApi spaceProbeApi = retrofit.create(SpaceProbeApi.class);

        source = new NetworkDirectionsSource(spaceProbeApi, "bryniarskip");
        mover = new Mover(new MoveLeft(), new MoveRight(), new MoveForward());
    }
}
